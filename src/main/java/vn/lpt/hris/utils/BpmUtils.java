package vn.lpt.hris.utils;

import lombok.extern.slf4j.Slf4j;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.UserTask;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class BpmUtils {

    public static List<UserTask> getUserTask(BpmnModel model) {
        List<Process> processes = model.getProcesses();
        List<UserTask> userTasks = new ArrayList<>();
        for (Process p : processes) {
            userTasks.addAll(p.findFlowElementsOfType(UserTask.class));
        }
        return userTasks;
    }


}
