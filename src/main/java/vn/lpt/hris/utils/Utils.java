package vn.lpt.hris.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import vn.lpt.hris.models.UserDTO;

import javax.security.sasl.AuthenticationException;
import java.util.ArrayList;

@Slf4j
public class Utils {

    private Utils() {
        throw new IllegalStateException("Utils class");
    }

    public static String tryToWriteObjectAsJsonString(ObjectMapper objectMapper, Object value) {
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);
            throw new IllegalArgumentException("Could not write object as json string");
        }
    }


    public static UserDTO getUser() {
        var securityContext = getKeycloakSecurityContext();
        var token = securityContext.getToken();
        return UserDTO.builder()
                .username(token.getPreferredUsername())
                .sessionState(token.getSessionState())
                .token(securityContext.getTokenString())
                .roles(new ArrayList<>(token.getRealmAccess().getRoles()))
                .build();
    }

    public static KeycloakSecurityContext getKeycloakSecurityContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null) throw new AuthorizationServiceException("Wrong authentication");
        if (!(authentication instanceof KeycloakAuthenticationToken))
            throw new AccessDeniedException("Wrong authentication");
        return ((KeycloakAuthenticationToken) authentication).getAccount().getKeycloakSecurityContext();
    }


    public static String appendLikeExpression(String value) {
        return String.format("%%%s%%", value);
    }
}
