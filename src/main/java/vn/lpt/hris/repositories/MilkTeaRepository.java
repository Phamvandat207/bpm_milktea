package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.lpt.hris.domains.MilkTea;

@Repository
public interface MilkTeaRepository extends JpaRepository<MilkTea, Integer>,
        PagingAndSortingRepository<MilkTea, Integer> {
}
