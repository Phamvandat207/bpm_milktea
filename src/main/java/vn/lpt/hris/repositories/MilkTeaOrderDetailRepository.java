package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import vn.lpt.hris.domains.MilkTeaOrderDetail;

@Repository
public interface MilkTeaOrderDetailRepository extends JpaRepository<MilkTeaOrderDetail, Integer>,
        PagingAndSortingRepository<MilkTeaOrderDetail, Integer> {
}
