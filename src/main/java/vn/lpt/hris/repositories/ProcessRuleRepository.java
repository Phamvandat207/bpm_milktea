package vn.lpt.hris.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import vn.lpt.hris.domains.ProcessRule;

import java.util.List;

public interface ProcessRuleRepository extends JpaRepository<ProcessRule, Integer> {
    List<ProcessRule> findAllByProcessKey(String processKey);
    ProcessRule findByProcessKey(String processKey);
}
