package vn.lpt.hris.feign;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import vn.lpt.hris.models.ApprovalInformationDTO;
import vn.lpt.hris.models.LeaveOfAbsenceApproveDTO;
import vn.lpt.hris.models.LeaveOfAbsenceCreateDTO;

import java.util.List;

@FeignClient(name = "hris-services", decode404 = true, url = "https://hris-apis.brostech.xyz")
public interface HrisServicesClient {

    @PostMapping(value = "/employees/leave-of-absence", produces = MediaType.APPLICATION_JSON_VALUE)
    Long create(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader,
            @RequestBody LeaveOfAbsenceCreateDTO dto);


    @GetMapping(value = "/employees/leave-of-absence/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Object search(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader,
            @PathVariable(value = "id") Long id);


    @PostMapping(value = "/employees/leave-of-absence/{id}/decision", produces = MediaType.APPLICATION_JSON_VALUE)
    void decision(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader,
            @RequestBody LeaveOfAbsenceApproveDTO dto,
            @PathVariable Long id);



    @GetMapping(value = "/employees/leave-of-absence/unit/{unitCode}/approve-list", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ApprovalInformationDTO> findByApprove(
            @RequestHeader(HttpHeaders.AUTHORIZATION) String authHeader,
            @PathVariable(value = "unitCode") String unitCode) ;

}
