package vn.lpt.hris.models;

import lombok.*;

import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MilkTeaNextTaskDTO {
    private String taskId;
    private Map<String, Object> variables;
}
