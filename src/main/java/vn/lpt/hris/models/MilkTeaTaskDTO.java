package vn.lpt.hris.models;

import lombok.*;

import java.util.Map;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MilkTeaTaskDTO{
    private String id;
    private String name;
    private String description;
    private Map<String, Object> variables;
}
