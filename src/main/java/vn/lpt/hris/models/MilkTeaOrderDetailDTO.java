package vn.lpt.hris.models;

import lombok.*;
import vn.lpt.hris.domains.MilkTeaOrderDetail;

@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MilkTeaOrderDetailDTO {
    private int id;
    private String customerName;
    private int milkTeaId;
    private int quantity;
    private float price;
    private float discount;
    private boolean status;

    public MilkTeaOrderDetailDTO(MilkTeaOrderDetail milkTeaOrderDetail){
        this.id = milkTeaOrderDetail.getId();
        this.customerName = milkTeaOrderDetail.getCustomerName();
        this.milkTeaId = milkTeaOrderDetail.getMilkTeaId();
        this.quantity = milkTeaOrderDetail.getQuantity();
        this.price = milkTeaOrderDetail.getPrice();
        this.discount = milkTeaOrderDetail.getDiscount();
        this.status = milkTeaOrderDetail.isStatus();
    }
}
