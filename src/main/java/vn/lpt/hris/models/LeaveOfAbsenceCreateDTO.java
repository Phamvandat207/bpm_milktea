package vn.lpt.hris.models;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LeaveOfAbsenceCreateDTO {
    @NonNull
    private String employeeCode;
    @NonNull
    private LocalDate startDate;
    @NonNull
    private LocalDate endDate;
    @NonNull
    private String reason;
}
