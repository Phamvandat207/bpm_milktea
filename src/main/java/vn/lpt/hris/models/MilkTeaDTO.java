package vn.lpt.hris.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.lpt.hris.domains.MilkTea;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MilkTeaDTO {
    private int id;
    private String name;
    private float price;

    public MilkTeaDTO(MilkTea milkTea){
        this.id = milkTea.getId();
        this.name = milkTea.getName();
        this.price = milkTea.getPrice();
    }
}
