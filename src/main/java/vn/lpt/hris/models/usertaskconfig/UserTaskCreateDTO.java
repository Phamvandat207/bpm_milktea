package vn.lpt.hris.models.usertaskconfig;

import lombok.*;
import org.activiti.bpmn.model.UserTask;

/**
 * Class map dữ liệu đầu ra khi tạo mới
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserTaskCreateDTO {

    private String userTaskId;
    private String name;
    private String config;
    private Integer orderNum;
    private String priority;

    public UserTaskCreateDTO(UserTask dto) {
        this.userTaskId = dto.getId();
        this.name = dto.getName();
        this.orderNum = dto.getXmlRowNumber();
        this.priority = dto.getPriority();
    }
}