package vn.lpt.hris.models;

import lombok.*;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class StartProcessModel {
    private String processDefinitionId;
    private String processInstanceId;
    private String taskId;
    private Map<String, Object> variables = new HashMap<>();
}
