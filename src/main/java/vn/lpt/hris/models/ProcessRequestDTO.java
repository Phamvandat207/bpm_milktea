package vn.lpt.hris.models;

import lombok.Builder;
import lombok.Data;
import org.activiti.api.process.model.payloads.StartProcessPayload;
import org.activiti.engine.task.Task;

import java.util.Date;
import java.util.List;
import java.util.Map;

/** @author HP */
@Data
@Builder
public class ProcessRequestDTO {
  private String taskId;
  private String type;
  private String status;
  private String processId;
  private String contactId;
}
