package vn.lpt.hris.models.processconfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;
import vn.lpt.hris.domains.ProcessConfig;
import vn.lpt.hris.models.usertaskconfig.UserTaskDTO;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class map dữ liệu đầu ra khi tạo mới
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class ProcessDTO {

    private Long id;
    private String processId;
    private String config;
    private String name;
    private List<UserTaskDTO> userTasks;
    private List<String> status;

    public ProcessDTO(ProcessConfig processConfig, ObjectMapper mapper) throws JsonProcessingException {
        this.id = processConfig.getId();
        this.processId = processConfig.getProcessId();
        this.config = processConfig.getConfig();
        this.name = processConfig.getName();
        this.userTasks = processConfig.getUserTasks().stream().map(UserTaskDTO::new)
                .sorted(Comparator.comparing(UserTaskDTO::getOrderNum))
                .collect(Collectors.toList());
        this.status = mapper.readValue(processConfig.getStatus(), new TypeReference<List<String>>() {
        });
    }
}