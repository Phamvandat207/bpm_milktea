package vn.lpt.hris.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.activiti.engine.task.TaskInfo;
import org.activiti.engine.task.TaskQuery;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Response {
  private String message;
  private String code;
  private List<?> data;
  private List<Error> errors;
  private Object lanes;
  private Object sidebars;
  private Object tasks;
  private Object contact;
  private Object account;
  private Object opportunity;
  private TaskQuery taskQuery;

  private TaskInfo taskInfo;
}
