package vn.lpt.hris.models;

import lombok.*;
import vn.lpt.hris.domains.ProcessRule;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProcessRuleDTO {
    private int id;
    private String processKey;
    private String processInstanceId;
    private String ruleLocation;

    public ProcessRuleDTO(ProcessRule processRule){
        this.id = processRule.getId();
        this.processKey = processRule.getProcessKey();
        this.processInstanceId = processRule.getProcessInstanceId();
        this.ruleLocation = processRule.getRuleLocation();
    }
}
