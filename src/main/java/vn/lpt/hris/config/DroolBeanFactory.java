package vn.lpt.hris.config;

import org.drools.decisiontable.DecisionTableProviderImpl;
import org.kie.api.KieServices;
import org.kie.api.builder.*;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.DecisionTableConfiguration;
import org.kie.internal.builder.DecisionTableInputType;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;

@Configuration
public class DroolBeanFactory {

    @Bean
    public KieContainer kieContainer(){
        KieServices kieServices = KieServices.Factory.get();

        KieFileSystem kfs = kieServices.newKieFileSystem();
        KieRepository kr = kieServices.getRepository();
        File file = new File("C://Users/D/Desktop/MilkTeaOrderRule.xlsx");
        Resource resource = kieServices.getResources().newFileSystemResource(file);
        kfs.write(resource);

        KieBuilder kb = kieServices.newKieBuilder(kfs);
        kb.buildAll();
        return kieServices.newKieContainer(kr.getDefaultReleaseId());
    }

    @Bean
    public KieSession KieSession() {
        System.out.println("session created...");
        return kieContainer().newKieSession();

    }
}
