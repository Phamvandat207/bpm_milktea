package vn.lpt.hris.exceptions;

import java.util.HashMap;
import java.util.Map;

public class ExceptionUtils {

  public static final String E_INTERNAL_SERVER = "E_INTERNAL_SERVER";
  public static final String E_COMMON_DUPLICATE_CODE = "E_COMMON_DUPLICATE_CODE";
  public static final String E_COMMON_NOT_EXISTS_CODE = "E_COMMON_NOT_EXISTS_CODE";
  public static final String E_NOT_MATCH_CODE = "E_NOT_MATCH_CODE";
  public static Map<String, String> messages;

  static {
    messages = new HashMap<>();
    messages.put(ExceptionUtils.E_INTERNAL_SERVER, "Server không phản hồi");
    messages.put(ExceptionUtils.E_COMMON_DUPLICATE_CODE, "Code %s đã tồn tại");
    messages.put(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE, "Code %s không tồn tại");
    messages.put(ExceptionUtils.E_NOT_MATCH_CODE, "Code %s không khớp");
  }

  public static String buildMessage(String messKey, Object... arg) {
    return String.format(ExceptionUtils.messages.get(messKey), arg);
  }
}