package vn.lpt.hris.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTea;
import vn.lpt.hris.repositories.MilkTeaRepository;
import vn.lpt.hris.services.MilkTeaService;

import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class MilkTeaServiceImpl implements MilkTeaService {

    private final MilkTeaRepository milkTeaRepository;

    public MilkTeaServiceImpl(MilkTeaRepository milkTeaRepository) {
        this.milkTeaRepository = milkTeaRepository;
    }

    @Override
    public MilkTea getMilkTea(int id) {
        return milkTeaRepository.findById(id).orElse(null);
    }

    @Override
    public List<MilkTea> getAllMilkTeas() {
        return milkTeaRepository.findAll();
    }

    @Override
    public MilkTea createMilkTea(MilkTea milkTea) {
        if (milkTeaRepository.existsById(milkTea.getId())) {
            return null;
        }
        milkTeaRepository.save(milkTea);
        return milkTea;
    }

    @Override
    public MilkTea updateMilkTea(MilkTea milkTea) {
        if (!milkTeaRepository.existsById(milkTea.getId())) {
            MilkTea current = milkTeaRepository.getById(milkTea.getId());
            current.setName(milkTea.getName());
            current.setPrice(milkTea.getPrice());
            milkTeaRepository.save(current);
            return current;
        }
        return null;
    }

    @Override
    public String deleteMilkTea(int id) {
        String msg;
        if (!milkTeaRepository.existsById(id)) {
            msg = "Không tìm thấy dữ liệu";
        }
        milkTeaRepository.deleteById(id);
        msg = "Xoá sản phẩm thành công!";
        return msg;
    }
}
