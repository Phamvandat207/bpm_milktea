package vn.lpt.hris.services.impl;

import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTeaOrderDetail;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.services.MilkTeaOrderDetailService;

import java.util.Map;

@Service("MilkTeaOrderConfirmImpl")
public class MilkTeaOrderConfirmImpl implements JavaDelegate {

    private final MilkTeaOrderDetailService milkTeaOrderDetailService;

    public MilkTeaOrderConfirmImpl(MilkTeaOrderDetailService milkTeaOrderDetailService) {
        this.milkTeaOrderDetailService = milkTeaOrderDetailService;
    }
    @Override
    public void execute(DelegateExecution execution) {
        Map<String, Object> processVariables = execution.getVariables();
        Object orderId = processVariables.get("id");
        Object status = processVariables.get("status");
        MilkTeaOrderDetailDTO milkTeaOrder = milkTeaOrderDetailService
                .getMilkTeaOrder((int) orderId);
        milkTeaOrder.setStatus((Boolean) status);
        milkTeaOrderDetailService.updateMilkTeaOrder(milkTeaOrder);
    }
}
