package vn.lpt.hris.services.impl;

import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTeaOrderDetail;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.repositories.MilkTeaOrderDetailRepository;
import vn.lpt.hris.services.MilkTeaOrderDetailService;

import java.util.List;

@Service
public class MilkTeaOrderDetailServiceImpl implements MilkTeaOrderDetailService {

    private final MilkTeaOrderDetailRepository milkTeaOrderDetailRepository;

    public MilkTeaOrderDetailServiceImpl(MilkTeaOrderDetailRepository milkTeaOrderDetailRepository) {
        this.milkTeaOrderDetailRepository = milkTeaOrderDetailRepository;
    }

    @Override
    public MilkTeaOrderDetailDTO getMilkTeaOrder(int id) {
        MilkTeaOrderDetail milkTeaOrderDetail = milkTeaOrderDetailRepository.findById(id).orElse(null);
        if (milkTeaOrderDetail != null) {
            return new MilkTeaOrderDetailDTO(milkTeaOrderDetail);
        }
        return null;
    }

    @Override
    public List<MilkTeaOrderDetail> getAllMilkTeasOrder() {
        return milkTeaOrderDetailRepository.findAll();
    }

    @Override
    public MilkTeaOrderDetail createMilkTeaOrder(MilkTeaOrderDetailDTO milkTeaOrderDetailDTO) {
        if (milkTeaOrderDetailRepository.existsById(milkTeaOrderDetailDTO.getId())){
            return null;
        }
        MilkTeaOrderDetail milkTeaOrderDetail = MilkTeaOrderDetail.builder()
                .id(milkTeaOrderDetailDTO.getId())
                .customerName(milkTeaOrderDetailDTO.getCustomerName())
                .milkTeaId(milkTeaOrderDetailDTO.getMilkTeaId())
                .price(milkTeaOrderDetailDTO.getPrice())
                .quantity(milkTeaOrderDetailDTO.getQuantity())
                .discount(milkTeaOrderDetailDTO.getDiscount())
                .status(milkTeaOrderDetailDTO.isStatus())
                .build();
        return milkTeaOrderDetailRepository.save(milkTeaOrderDetail);
    }

    @Override
    public MilkTeaOrderDetailDTO updateMilkTeaOrder(MilkTeaOrderDetailDTO milkTeaOrderDto) {
        MilkTeaOrderDetail milkTeaOrder = milkTeaOrderDetailRepository.getById(milkTeaOrderDto.getId());
        milkTeaOrder.setPrice(milkTeaOrderDto.getPrice());
        milkTeaOrder.setStatus(milkTeaOrderDto.isStatus());
        MilkTeaOrderDetail save = milkTeaOrderDetailRepository.save(milkTeaOrder);
        return new MilkTeaOrderDetailDTO(save);
    }

    @Override
    public String deleteMilkTeaOrder(int id) {
        String msg;
        if (!milkTeaOrderDetailRepository.existsById(id)) {
            msg = "Không tìm thấy dữ liệu";
        }
        milkTeaOrderDetailRepository.deleteById(id);
        msg = "Xoá đơn hàng thành công!";
        return msg;
    }
}
