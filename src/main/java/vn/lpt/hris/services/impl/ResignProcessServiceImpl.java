package vn.lpt.hris.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.ResignProcess;
import vn.lpt.hris.repositories.ResignProcessRepository;
import vn.lpt.hris.services.ResignProcessService;
import vn.lpt.hris.utils.Utils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResignProcessServiceImpl implements ResignProcessService {

    private final ResignProcessRepository resignProcessRepository;


    public Page<ResignProcess> search(
            String assignee,
            String owner,
            String ownerName,
            String status,
            Pageable pageable) {
        if (pageable == null) {
            pageable = PageRequest.of(0, Integer.MAX_VALUE);
        }
        Specification<ResignProcess> specification =
                (root, criteriaQuery, criteriaBuilder) -> {
                    List<Predicate> predicates = new ArrayList<>();
                    if (StringUtils.isNotBlank(assignee)) {
                        predicates.add(criteriaBuilder.equal(root.get("assignee"), assignee));
                    }
                    if (StringUtils.isNotBlank(owner)) {
                        predicates.add(criteriaBuilder.equal(root.get("owner"), owner));
                    }
                    if (StringUtils.isNotBlank(ownerName)) {
                        predicates.add(criteriaBuilder.like(root.get("ownerName"),
                                Utils.appendLikeExpression(ownerName)));
                    }
                    if (StringUtils.isNotBlank(status)) {
                        predicates.add(criteriaBuilder.equal(root.get("status"), status));
                    }
                    return criteriaQuery.where(predicates.toArray(new Predicate[0])).getRestriction();
                };
        return resignProcessRepository.findAll(specification, pageable);
    }
}