package vn.lpt.hris.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.keycloak.OAuth2Constants;
import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.stereotype.Service;
import vn.lpt.hris.services.KeycloakService;

@Service
@RequiredArgsConstructor
@Slf4j
public class KeycloakServiceImpl implements KeycloakService {
    private final KeycloakSpringBootProperties properties;

    @Override
    public Keycloak getServiceAccountKeycloak() {
        return KeycloakBuilder.builder()
                .grantType(OAuth2Constants.CLIENT_CREDENTIALS)
                .clientId(properties.getResource())
                .serverUrl(properties.getAuthServerUrl())
                .realm(properties.getRealm())
                .clientSecret((String) properties.getCredentials().get("secret"))
                .build();
    }

    @Override
    public String getServiceAccountAccessToken() {
        return getServiceAccountAccessToken(false);
    }

    @Override
    public String getServiceAccountAccessToken(boolean includedBearer) {
        return StringUtils.join(
                includedBearer ? "Bearer " : StringUtils.EMPTY,
                getServiceAccountKeycloak().tokenManager().getAccessTokenString());
    }
}
