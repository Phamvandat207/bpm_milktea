package vn.lpt.hris.services.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.process.runtime.ProcessRuntime;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.runtime.ProcessInstanceBuilder;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.PageImpl;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.exceptions.ExceptionUtils;
import vn.lpt.hris.feign.HrisServicesClient;
import vn.lpt.hris.models.*;
import vn.lpt.hris.repositories.ResignProcessRepository;
import vn.lpt.hris.repositories.UserTaskConfigRepository;
import vn.lpt.hris.services.ProcessService;
import vn.lpt.hris.utils.Utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

@Service("ProcessService")
@RequiredArgsConstructor
@Slf4j
public class ProcessServiceImpl implements ProcessService {
    private final ProcessRuntime processRuntime;
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final RepositoryService repositoryService;
    private final UserTaskConfigRepository userTaskConfigRepository;
    private final ResignProcessRepository resignProcessRepository;
    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private final HrisServicesClient hrisServicesClient;

    private static void includeVariables(
            @NonNull TaskQuery query,
            boolean includeTaskLocalVariables,
            boolean includeProcessVariables) {
        if (includeProcessVariables) {
            query.includeProcessVariables();
        }
        if (includeTaskLocalVariables) {
            query.includeTaskLocalVariables();
        }
    }

    @Override
    public Page<ProcessInstance> getInstances(Pageable pageable) {
        return processRuntime.processInstances(pageable);
    }

    @Override
    public List<ProcessDefinition> getDefinitions() {
        var actPageable = Pageable.of(0, Integer.MAX_VALUE);
        return processRuntime.processDefinitions(actPageable).getContent();
    }

    @Override
    public String startProcess(StartProcessModel model) throws ParseException {
        UserDTO user = Utils.getUser();
        String token = String.format("Bearer %s", user.getToken());
        log.info(user.getUsername());
        Map<String, Object> variables = model.getVariables();
        String employeeCode = user.getUsername().toUpperCase();
        Date startDate = df.parse((String) variables.get("startDate"));
        LocalDate startDateLocal = LocalDate.ofInstant(startDate.toInstant(), ZoneId.systemDefault());
        Date endDate = df.parse((String) variables.get("endDate"));
        LocalDate endDateLocal = LocalDate.ofInstant(endDate.toInstant(), ZoneId.systemDefault());
        Map<String, Object> processVariables = new HashMap<>();
        // call api Create đơn nghỉ POST: /leave-of-absence
        // lấy được leave-of-absence-id
        Long leaveAbsenceId = hrisServicesClient.create(token, LeaveOfAbsenceCreateDTO.builder().employeeCode(employeeCode).startDate(startDateLocal).endDate(endDateLocal).reason((String) variables.get("reason")).build());
        // call data base lấy config step - đưa front end hiển thị .
        ProcessInstanceBuilder builder = runtimeService.createProcessInstanceBuilder();
        processVariables.put("startDateVariable", variables.get("startDate"));
        processVariables.put("endDateVariable", variables.get("endDate"));
        // lấy thông tin người phê duyệt
        processVariables.put("unitCode", variables.get("unitCode"));
        processVariables.put("leaveAbsenceId", leaveAbsenceId);
        processVariables.put("owner", user.getUsername().toUpperCase());
        builder.processDefinitionId(model.getProcessDefinitionId());
        builder.variables(processVariables);
        String processInstanceId = builder.start().getProcessInstanceId();
        Task task = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .singleResult(); //doan nay phai ko anh?
        return task.getId();
    }

//


    @Override
    public String nextTask(StartProcessModel payload) {
        Task task = taskService.createTaskQuery()
                .taskId(payload.getTaskId())
                .singleResult();
        log.info("{}", task);
        String processInstanceId = task.getProcessInstanceId();
        taskService.complete(task.getId(), payload.getVariables());
        Task taskCurrent = taskService.createTaskQuery()
                .processInstanceId(processInstanceId).singleResult();
        if (taskCurrent != null) {
            taskService.setVariableLocal(taskCurrent.getId(), "taskId", task.getId());
            return taskCurrent.getId();
        }
        return StringUtils.EMPTY;
    }

    @Override
    public org.springframework.data.domain.Page<TaskCrmDTO> getAllTaskByProcess(String processDefinitionId, String type, org.springframework.data.domain.Pageable pageable) {
        UserDTO user = Utils.getUser();
        TaskQuery taskQuery = taskService.createTaskQuery().processDefinitionId(processDefinitionId);
        taskQuery.or();
        taskQuery.taskAssignee(user.getUsername().toUpperCase());
        taskQuery.taskOwner(user.getUsername().toUpperCase());
        taskQuery.endOr();
        includeVariables(taskQuery, true, true);
        List<Task> taskList = taskQuery.active().list();
        List<TaskCrmDTO> taskCrmDTOS = new ArrayList<>();
        TaskCrmDTO crmDTO;
        for (Task task : taskList) {
            crmDTO = new TaskCrmDTO();
            crmDTO.setId(task.getId());
            crmDTO.setTaskName(task.getName());
            crmDTO.setOwner(task.getOwner());
            Map<String, Object> processVariables = task.getProcessVariables();
            Map<String, Object> taskVariable = task.getTaskLocalVariables();
            String startDate = (String) processVariables.get("startDateVariable");
            String endDate = (String) processVariables.get("endDateVariable");
            String reason = (String) taskVariable.get("reason");
            crmDTO.setStartDate(startDate);
            crmDTO.setEndDate(endDate);
            crmDTO.setReason(reason);
            crmDTO.setAssignee(task.getAssignee());
            taskCrmDTOS.add(crmDTO);
        }
        int count = taskCrmDTOS.size();
        List<TaskCrmDTO> content = taskCrmDTOS.stream().skip(pageable.getOffset()).limit(pageable.getPageSize()).collect(Collectors.toList());
        return new PageImpl<TaskCrmDTO>(content, pageable, count);
    }

    @Override
    public BpmnModel getbpmn(String processDefinitionId) {
        BpmnModel model = repositoryService.getBpmnModel(processDefinitionId);
        return model;
    }

    @Override
    public org.activiti.engine.repository.ProcessDefinition getDefinition(
            String processDefinitionId) throws BpmException {
        try {
            return repositoryService.getProcessDefinition(
                    processDefinitionId);
        } catch (ActivitiObjectNotFoundException ignore) {
            throw new BpmException(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE,
                    String.format(ExceptionUtils.messages.get(ExceptionUtils.E_COMMON_NOT_EXISTS_CODE),
                            processDefinitionId));
        }
    }

    public void approve(String approver, boolean isFinal, String approverNote, String decision, long leaveAbsenceId) {
        log.info("approver  :" + approver);
        log.info("approverNote  :" + approverNote);
        log.info("decision  :" + decision);
        log.info("isFinal  :" + isFinal);
        UserDTO user = Utils.getUser();
        String auth = String.format("Bearer %s", user.getToken());
        hrisServicesClient.decision(auth, LeaveOfAbsenceApproveDTO.builder().approver(approver.toUpperCase()).isFinal(isFinal).approverNote(approverNote).decision(decision).build(), leaveAbsenceId);

    }

    public void getApprover() {
        Execution execution = runtimeService.createExecutionQuery().processDefinitionId("").singleResult();
    }


    public boolean leaveAbsenceDay(String startDateVariable, String endDateVariable) throws ParseException {
        // tính ngày nghỉ
        Date startDate = df.parse(startDateVariable);
        Date endDate = df.parse(endDateVariable);
        int diffInDays = (int) ((endDate.getTime() - startDate.getTime())
                / (1000 * 60 * 60 * 24));
        log.info("số ngày nghỉ {}", diffInDays);
        return diffInDays <= 3;
    }
}
