package vn.lpt.hris.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceBuilder;
import org.activiti.engine.task.Task;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vn.lpt.hris.models.MilkTeaNextTaskDTO;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.models.MilkTeaTaskDTO;
import vn.lpt.hris.models.ProcessRuleDTO;
import vn.lpt.hris.services.MilkTeaOrderDetailService;
import vn.lpt.hris.services.MilkTeaProcessService;
import vn.lpt.hris.services.ProcessRuleService;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class MilkTeaProcessServiceImpl implements MilkTeaProcessService {
    private final RuntimeService runtimeService;
    private final TaskService taskService;
    private final ProcessRuleService processRuleService;
    private final MilkTeaOrderDetailService milkTeaOrderDetailService;

    public MilkTeaProcessServiceImpl(RuntimeService runtimeService,
                                     TaskService taskService,
                                     ProcessRuleService processRuleService,
                                     MilkTeaOrderDetailService milkTeaOrderDetailService) {
        this.runtimeService = runtimeService;
        this.taskService = taskService;
        this.processRuleService = processRuleService;
        this.milkTeaOrderDetailService = milkTeaOrderDetailService;
    }

    @Override
    public String startMilkTeaProcess(MilkTeaOrderDetailDTO milkTeaOrderDetailDTO) {
        String processKey = "MilkTea-Process";
        ProcessRuleDTO processRule = processRuleService.getProcessRule(processKey);

        //get rule file from dir
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(processRule.getRuleLocation());
        } catch (java.io.FileNotFoundException e) {
            e.printStackTrace();
        }
        //add a set off rule in the rule file from the dir above
        SpreadsheetCompiler sc = new SpreadsheetCompiler();
        String rules = sc.compile(inputStream, InputType.XLS);
        StringBuffer drl = new StringBuffer(rules);
        log.info("drool file == " + drl);

        //Create new Kie session using new rule
        KieSession kieSession = new KieHelper().addContent(drl.toString(), ResourceType.DRL).build().newKieSession();

        //Fire rule to get the value of discount
        kieSession.insert(milkTeaOrderDetailDTO);
        kieSession.fireAllRules();

        //Create ProcessInstance then get its processInstanceId
        ProcessInstanceBuilder builder = runtimeService.createProcessInstanceBuilder();
        Map<String, Object> processVariables = new HashMap<>();
        processVariables.put("id", milkTeaOrderDetailDTO.getId());
        processVariables.put("customerName", milkTeaOrderDetailDTO.getCustomerName());
        processVariables.put("milkTeaId", milkTeaOrderDetailDTO.getMilkTeaId());
        processVariables.put("quantity", milkTeaOrderDetailDTO.getQuantity());
        processVariables.put("price", milkTeaOrderDetailDTO.getPrice());
        processVariables.put("discount", milkTeaOrderDetailDTO.getDiscount());
        processVariables.put("status", milkTeaOrderDetailDTO.isStatus());
        builder.processDefinitionKey(processKey);
        builder.variables(processVariables);
        ProcessInstance start = builder.start();
        String processInstanceId = start.getId();

        Task task = taskService.createTaskQuery()
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .processInstanceId(processInstanceId)
                .singleResult();
        log.info(task.getId());

        //create a row in Order Detail table
        milkTeaOrderDetailService.createMilkTeaOrder(milkTeaOrderDetailDTO);
        return task.getId();
    }

    @Override
    @Transactional
    public String nextStep(MilkTeaNextTaskDTO milkTeaNextTaskDTO) {
        String response;
        Task task = taskService.createTaskQuery()
                .taskId(milkTeaNextTaskDTO.getTaskId())
                .includeProcessVariables()
                .includeTaskLocalVariables()
                .singleResult();
        String processInstanceId = task.getProcessInstanceId();
        Map<String, Object> processVariables = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .includeProcessVariables()
                .singleResult()
                .getProcessVariables();
        MilkTeaOrderDetailDTO milkTeaOrder = milkTeaOrderDetailService
                .getMilkTeaOrder((Integer) processVariables.get("id"));
        milkTeaOrder.setPrice((Float) processVariables.get("price"));
        if (milkTeaNextTaskDTO.getVariables() != null) {
            processVariables.put("status", milkTeaNextTaskDTO.getVariables().get("status"));
            taskService.complete(task.getId(), processVariables);
        } else {
            taskService.complete(task.getId());
        }

        Task nextTask = taskService.createTaskQuery()
                .processInstanceId(processInstanceId)
                .includeProcessVariables()
                .singleResult();
        if (nextTask != null) {
            String nextTaskId = nextTask.getId();
            response = "Id của Task tiếp theo là: " + nextTaskId;
        } else {
            return null;
        }
        return response;
    }

    @Override
    public MilkTeaTaskDTO getTaskById(String taskId) {
        Task task = taskService.createTaskQuery().includeProcessVariables().taskId(taskId).singleResult();
        MilkTeaTaskDTO milkTeaTaskDTO = new MilkTeaTaskDTO();
        milkTeaTaskDTO.setId(task.getId());
        milkTeaTaskDTO.setName(task.getName());
        milkTeaTaskDTO.setVariables(task.getProcessVariables());
        return milkTeaTaskDTO;
    }
}
