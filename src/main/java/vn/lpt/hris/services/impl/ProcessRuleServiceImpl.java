package vn.lpt.hris.services.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.lpt.hris.domains.ProcessRule;
import vn.lpt.hris.models.ProcessRuleDTO;
import vn.lpt.hris.repositories.ProcessRuleRepository;
import vn.lpt.hris.services.FileStorageService;
import vn.lpt.hris.services.ProcessRuleService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProcessRuleServiceImpl implements ProcessRuleService {

    @Value("${file.upload-dir}")
    private static String filePath;
    private final ProcessRuleRepository processRuleRepository;
    private final FileStorageService fileStorageService;

    public ProcessRuleServiceImpl(ProcessRuleRepository processRuleRepository, FileStorageServiceImpl fileStorageService) {
        this.processRuleRepository = processRuleRepository;
        this.fileStorageService = fileStorageService;
    }


    @Override
    public List<ProcessRuleDTO> getAllProcessRule() {
        List<ProcessRule> allProcessRule = processRuleRepository.findAll();
        List<ProcessRuleDTO> allProcessRuleDTO = new ArrayList<>();
        for (ProcessRule processRule : allProcessRule) {
            ProcessRuleDTO build = ProcessRuleDTO.builder()
                    .id(processRule.getId())
                    .processKey(processRule.getProcessKey())
                    .processInstanceId(processRule.getProcessInstanceId())
                    .ruleLocation(processRule.getRuleLocation())
                    .build();
            allProcessRuleDTO.add(build);
        }
        return allProcessRuleDTO;
    }

    @Override
    public List<ProcessRuleDTO> getProcessAllRuleByProcessKey(String processKey) {
        List<ProcessRule> allByProcessKey = processRuleRepository.findAllByProcessKey(processKey);
        List<ProcessRuleDTO> allProcessRuleByKeyDTO = new ArrayList<>();
        for (ProcessRule processRule : allByProcessKey) {
            ProcessRuleDTO build = ProcessRuleDTO.builder()
                    .id(processRule.getId())
                    .processKey(processRule.getProcessKey())
                    .processInstanceId(processRule.getProcessInstanceId())
                    .ruleLocation(processRule.getRuleLocation())
                    .build();
            allProcessRuleByKeyDTO.add(build);
        }
        return allProcessRuleByKeyDTO;
    }

    @Override
    public ProcessRuleDTO getProcessRule(String processKey) {
        ProcessRule processRule = processRuleRepository.findByProcessKey(processKey);
        return ProcessRuleDTO.builder()
                .id(processRule.getId())
                .processKey(processRule.getProcessKey())
                .processInstanceId(processRule.getProcessInstanceId())
                .ruleLocation(processRule.getRuleLocation())
                .build();
    }

    @Override
    public ProcessRuleDTO createProcessRule(MultipartFile multipartFile, String processKey) {
        String fileName = fileStorageService.storeFile(multipartFile);
        ProcessRuleDTO processRuleDTOBuilder = ProcessRuleDTO.builder()
                .ruleLocation(filePath + "/" + fileName)
                .processKey(processKey).build();
        ProcessRule processRuleBuilder = ProcessRule.builder()
                .processKey(processRuleDTOBuilder.getProcessKey())
                .processInstanceId(processRuleDTOBuilder.getProcessInstanceId())
                .ruleLocation(processRuleDTOBuilder.getRuleLocation())
                .build();
        processRuleRepository.save(processRuleBuilder);
        return processRuleDTOBuilder;
    }

    @Override
    public ProcessRuleDTO updateProcessRule() {
        return null;
    }

    @Override
    public void deleteProcessRule() {

    }
}
