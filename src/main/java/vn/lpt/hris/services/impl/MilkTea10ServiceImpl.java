package vn.lpt.hris.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.delegate.DelegateExecution;
import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTea;
import vn.lpt.hris.domains.MilkTeaOrderDetail;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.services.MilkTea10Service;
import vn.lpt.hris.services.MilkTeaOrderDetailService;
import vn.lpt.hris.services.MilkTeaService;

@Slf4j
@Service("MilkTea10ServiceImpl")
public class MilkTea10ServiceImpl implements MilkTea10Service {

    private final MilkTeaService milkTeaService;
    private final MilkTeaOrderDetailService milkTeaOrderDetailService;

    public MilkTea10ServiceImpl(MilkTeaService milkTeaService, MilkTeaOrderDetailService milkTeaOrderDetailService) {
        this.milkTeaService = milkTeaService;
        this.milkTeaOrderDetailService = milkTeaOrderDetailService;
    }

    @Override
    public void execute(DelegateExecution execution) {
        int orderId = (Integer) execution.getVariable("id");
        int quantity = (Integer) execution.getVariable("quantity");
        int milkTeaId = (Integer) execution.getVariable("milkTeaId");
        float discount = (Float) execution.getVariable("discount");
        String processInstanceId = execution.getProcessInstanceId();
        log.info("Đã đặt :" + execution.getVariable("quantity") + " cốc trà sữa");
        MilkTeaOrderDetailDTO milkTeaOrder = milkTeaOrderDetailService.getMilkTeaOrder(orderId);
        MilkTea milkTea = milkTeaService.getMilkTea(milkTeaId);
        float basePrice = milkTea.getPrice();
        float orderPrice = basePrice * quantity - (basePrice * quantity * (discount/100));
        execution.setVariable("price", orderPrice);
        milkTeaOrder.setPrice(orderPrice);
        milkTeaOrderDetailService.updateMilkTeaOrder(milkTeaOrder);

        log.info("processInstanceId: " +processInstanceId);
        log.info("Giá trị đơn hàng: " +orderPrice+ " vnd");
    }
}
