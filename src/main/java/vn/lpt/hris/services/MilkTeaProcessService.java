package vn.lpt.hris.services;

import org.springframework.stereotype.Service;
import vn.lpt.hris.models.MilkTeaNextTaskDTO;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.models.MilkTeaTaskDTO;

@Service
public interface MilkTeaProcessService {
    String startMilkTeaProcess(MilkTeaOrderDetailDTO milkTeaOrderDetailDTO);

    String nextStep(MilkTeaNextTaskDTO milkTeaNextTaskDTO);

    MilkTeaTaskDTO getTaskById(String taskId);
}
