package vn.lpt.hris.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import vn.lpt.hris.config.FileStorageProperties;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public interface FileStorageService {
    String storeFile(MultipartFile file);
    Resource loadFileAsResource(String fileName);

}
