package vn.lpt.hris.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.models.ProcessConfigDTO;
import vn.lpt.hris.models.processconfig.ProcessCreateDTO;
import vn.lpt.hris.models.processconfig.ProcessDTO;

public interface ProcessConfigService {

    /**
     * Save ProcessConfig
     *
     * @author KhanhNM
     * @since 13/02/2022
     */
    void update(ProcessCreateDTO dto, Long id) throws BpmException;

    /**
     * Find By ProcessId
     *
     * @author KhanhNM
     * @since 13/02/2022
     */
    ProcessDTO findByProcessId(String processId) throws BpmException, JsonProcessingException;

    void updateProcessConfig(ProcessConfigDTO dto) throws BpmException;

    void updateStepConfig(ProcessConfigDTO dto) throws BpmException;

}