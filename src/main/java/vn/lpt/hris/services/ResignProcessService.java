package vn.lpt.hris.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import vn.lpt.hris.domains.ResignProcess;

public interface ResignProcessService {

  Page<ResignProcess> search(
      String assignee,
      String owner,
      String ownerName,
      String status,
      Pageable pageable);

}