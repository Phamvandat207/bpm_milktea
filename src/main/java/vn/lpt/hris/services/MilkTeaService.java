package vn.lpt.hris.services;

import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTea;

import java.util.List;
import java.util.Optional;

@Service
public interface MilkTeaService {
    MilkTea getMilkTea(int id);
    List<MilkTea> getAllMilkTeas();
    MilkTea createMilkTea(MilkTea milkTea);
    MilkTea updateMilkTea(MilkTea milkTea);
    String deleteMilkTea(int id);

}
