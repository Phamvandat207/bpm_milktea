package vn.lpt.hris.services;

import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.activiti.api.runtime.shared.query.Page;
import org.activiti.api.runtime.shared.query.Pageable;
import org.activiti.bpmn.model.BpmnModel;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.models.StartProcessModel;
import vn.lpt.hris.models.TaskCrmDTO;

import java.text.ParseException;
import java.util.List;

public interface ProcessService {
    /**
     * @param pageable Pageable
     * @return page of ProcessInstance
     */
    Page<ProcessInstance> getInstances(Pageable pageable);

    /**
     * @return list of ProcessDefinition
     */
    List<ProcessDefinition> getDefinitions();

    /**
     * @param payload process payload
     * @return ProcessInstance
     */
    String startProcess(StartProcessModel payload) throws ParseException;

    String nextTask(StartProcessModel payload);


    org.springframework.data.domain.Page<TaskCrmDTO> getAllTaskByProcess(String processDefinitionId, String type, org.springframework.data.domain.Pageable pageable);

    BpmnModel getbpmn(String processDefinitionId);

    org.activiti.engine.repository.ProcessDefinition getDefinition(String processDefinitionId)
            throws BpmException;

}
