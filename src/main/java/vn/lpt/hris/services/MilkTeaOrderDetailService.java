package vn.lpt.hris.services;

import org.springframework.stereotype.Service;
import vn.lpt.hris.domains.MilkTeaOrderDetail;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;

import java.util.List;

@Service
public interface MilkTeaOrderDetailService {
    MilkTeaOrderDetailDTO getMilkTeaOrder(int id);
    List<MilkTeaOrderDetail> getAllMilkTeasOrder();
    MilkTeaOrderDetail createMilkTeaOrder(MilkTeaOrderDetailDTO milkTeaOrder);
    MilkTeaOrderDetailDTO updateMilkTeaOrder(MilkTeaOrderDetailDTO milkTeaOrderDetailDTO);
    String deleteMilkTeaOrder(int id);
}
