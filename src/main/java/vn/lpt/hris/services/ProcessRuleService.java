package vn.lpt.hris.services;

import org.springframework.web.multipart.MultipartFile;
import vn.lpt.hris.models.ProcessRuleDTO;

import java.util.List;

public interface ProcessRuleService {
    List<ProcessRuleDTO> getAllProcessRule();
    List<ProcessRuleDTO> getProcessAllRuleByProcessKey(String processKey);
    ProcessRuleDTO getProcessRule(String processKey);
    ProcessRuleDTO createProcessRule(MultipartFile multipartFile, String processKey);
    ProcessRuleDTO updateProcessRule();
    void deleteProcessRule();
}
