package vn.lpt.hris.listeners;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import java.util.Map;

public class ApproveLevel2Handler implements TaskListener {
    @Override
    public void notify(DelegateTask delegateTask) {
        Map<String, Object> processVariables = delegateTask.getExecution().getParent().getVariables();
        delegateTask.setAssignee((String)processVariables.get("approver2"));
        delegateTask.setOwner((String)processVariables.get("owner"));
    }
}