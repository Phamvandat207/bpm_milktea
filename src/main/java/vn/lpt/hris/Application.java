package vn.lpt.hris;

import co.elastic.apm.attach.ElasticApmAttacher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import vn.lpt.hris.config.DroolRulesReloadConfig;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@SpringBootApplication
@EnableWebSecurity
@EnableCaching
@EnableFeignClients
public class Application {

  public static void main(String[] args) throws IOException {
    ElasticApmAttacher.attach();
    SpringApplication.run(Application.class, args);
    Path dir = Paths.get("C:/Users/D/IdeaProjects/hris-bpm/src/main/resources/rules");
    new DroolRulesReloadConfig(dir).processEvents();

  }
}
