package vn.lpt.hris.shared.configurations;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import vn.lpt.hris.listeners.ActivityEventListener;
import vn.lpt.hris.repositories.ResignProcessRepository;

@Configuration
public class TaskRuntimeCustomConfiguration {
    @Bean
    public InitializingBean createTaskCreatedEventListener(
            RuntimeService runtimeService, ResignProcessRepository resignProcessRepository) {
        return () ->
                runtimeService.addEventListener(
                        new ActivityEventListener(resignProcessRepository), ActivitiEventType.TASK_CREATED);
    }
    @Bean
    public InitializingBean createTaskCompleteEventListener(
            RuntimeService runtimeService, ResignProcessRepository resignProcessRepository) {
        return () ->
                runtimeService.addEventListener(
                        new ActivityEventListener(resignProcessRepository), ActivitiEventType.TASK_COMPLETED);
    }

    @Bean
    public InitializingBean createTaskDeleteEventListener(
            RuntimeService runtimeService, ResignProcessRepository resignProcessRepository) {
        return () ->
                runtimeService.addEventListener(
                        new ActivityEventListener(resignProcessRepository), ActivitiEventType.ENTITY_DELETED);
    }
}
