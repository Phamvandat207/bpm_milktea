package vn.lpt.hris.domains;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "process_rule")
public class ProcessRule {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "process_key")
    private String processKey;
    @Column(name = "process_instance_id")
    private String processInstanceId;
    @Column(name = "rule_location")
    private String ruleLocation;
}
