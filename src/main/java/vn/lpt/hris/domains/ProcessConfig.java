package vn.lpt.hris.domains;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Class UserTaskConfig
 *
 * @author KhanhNM
 * @since 13/02/2022
 */
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
@Builder
@Table(name = "process_config")
public class ProcessConfig implements Serializable {

    @Id
    @SequenceGenerator(
            name = "process_config_id_sequence",
            sequenceName = "process_config_id_sequence",
            allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "process_config_id_sequence")
    @Column(name = "id", nullable = false)
    private Long id;
    @Column(name = "process_id")
    private String processId;
    @Column(name = "name")
    private String name;
    @Column(name = "config")
    private String config;
    private String status;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "processConfig", cascade = CascadeType.ALL)
    private Set<UserTaskConfig> userTasks;


}