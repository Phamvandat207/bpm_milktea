package vn.lpt.hris.domains;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "milk_tea_order_detail")
public class MilkTeaOrderDetail {
    @Id
    @Column(name = "id", nullable = false)
    private int id;
    @Column(name = "customer_name")
    private String customerName;
    @Column(name = "milk_tea_id")
    private int milkTeaId;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "price")
    private float price;
    @Column(name = "discount")
    private float discount;
    @Column(name = "status")
    private boolean status;
}
