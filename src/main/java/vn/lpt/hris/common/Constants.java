package vn.lpt.hris.common;

/**
 * Class định nghĩa các hằng số
 *
 * @author khanhnt
 * @since 1/12/2021
 */
public class Constants {

  public static final String MESS_COMPLETE_SUCCESS = "Complete tasks success!";
  public static final String TYPE = "type";

  public static final String ID = "id";
  public static final String PROCESS_ID = "processId";

  private Constants() {
    throw new IllegalStateException("Constant class");
  }
}
