package vn.lpt.hris.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import vn.lpt.hris.models.ProcessRuleDTO;
import vn.lpt.hris.services.ProcessRuleService;

@RestController
@Slf4j
@RequestMapping("/process-rule")
public class ProcessRuleController {

    private final ProcessRuleService processRuleService;

    public ProcessRuleController(ProcessRuleService processRuleService) {
        this.processRuleService = processRuleService;
    }

    @PostMapping("/newProcessWithRule")
    public ProcessRuleDTO newProcessWithRule(@RequestParam("file") MultipartFile file, @RequestParam("processKey") String processKey) {
        return processRuleService.createProcessRule(file, processKey);
    }
}
