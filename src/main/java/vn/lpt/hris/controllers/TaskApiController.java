package vn.lpt.hris.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.task.model.Task;
import org.activiti.api.task.model.payloads.CompleteTaskPayload;
import org.activiti.engine.task.TaskInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.models.TaskCrmDTO;
import vn.lpt.hris.models.TaskInfoDTO;
import vn.lpt.hris.models.common.ErrorDTO;
import vn.lpt.hris.models.usertaskconfig.UserTaskCreateDTO;
import vn.lpt.hris.models.usertaskconfig.UserTaskDTO;
import vn.lpt.hris.services.TaskService;
import vn.lpt.hris.services.UserTaskConfigService;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/tasks")
@RequiredArgsConstructor
@Slf4j
public class TaskApiController {

    private final TaskService taskService;
    private final UserTaskConfigService userTaskConfigService;

    private final org.activiti.engine.TaskService taskServiceEngine;
    /**
     * @author lptech
     * @apiNote : api get task by Id
     * @since 02/02/2020
     */
    @GetMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskCrmDTO findById(
            @PathVariable String id,
            @RequestParam(required = false) boolean includeTaskLocalVariables,
            @RequestParam(required = false) boolean includeProcessVariables)
            throws IllegalAccessException, ClassNotFoundException {
        return taskService.getTaskById(true,true,id);
    }

    /**
     * @author lptech
     * @apiNote : api get all task
     * @since 02/02/2020
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TaskInfo> findAll(
            @RequestParam(required = false) String[] types,
            @RequestParam(required = false) boolean includeTaskLocalVariables,
            @RequestParam(required = false) boolean includeProcessVariables)
            throws IllegalAccessException {
        return taskService.getTasks(types, includeTaskLocalVariables, includeProcessVariables).stream()
                .map(TaskInfoDTO::new)
                .collect(Collectors.toList());
    }

    /**
     * api complete task when change task process opportunity
     *
     * @author lptech
     * @apiNote : api complete task when change task process opportunity
     * @since 02/02/2020
     */
    @PostMapping("/complete")
    public Task complete(CompleteTaskPayload payload) {
        return taskService.complete(payload);
    }


    /**
     * Xem chi tiết process definition
     *
     * @param processDefinitionId mã
     * @return ProcessConfig
     * @author KhanhNM
     * @since 13/02/2022
     */
    @GetMapping(value = "/config/{processDefinitionId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDefinition(@PathVariable String processDefinitionId) {
        try {
            // Lấy chi tiết
            List<UserTaskDTO> userTaskDTOS = userTaskConfigService.findByProcessId(processDefinitionId);
            return new ResponseEntity<>(userTaskDTOS, HttpStatus.OK);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Cập nhật ProcessConfig
     *
     * @param id  mã
     * @param dto model update
     * @author KhanhNM
     * @since 13/02/2022
     */
    @PutMapping(value = "/config/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateDefinition(@PathVariable Long id, @RequestBody
            UserTaskCreateDTO dto) {
        try {
            // Cập nhật
            userTaskConfigService.update(dto, id);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }


    @GetMapping(value = "/step-by-task", produces = APPLICATION_JSON_VALUE)
    public List<UserTaskDTO> stepByTask(@RequestParam String taskId, @RequestParam String processId) {
        return userTaskConfigService.stepByTask(taskId,processId);
    }
}
