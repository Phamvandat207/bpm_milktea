package vn.lpt.hris.controllers;

import lombok.extern.slf4j.Slf4j;
import org.activiti.engine.TaskService;
import org.springframework.web.bind.annotation.*;
import vn.lpt.hris.models.MilkTeaNextTaskDTO;
import vn.lpt.hris.models.MilkTeaOrderDetailDTO;
import vn.lpt.hris.models.MilkTeaTaskDTO;
import vn.lpt.hris.services.MilkTeaProcessService;

@RestController
@Slf4j
@RequestMapping("/milkTea-Process")
public class MilkTeaProcessApiController {
    private final MilkTeaProcessService milkTeaProcessService;
    private final TaskService taskService;

    public MilkTeaProcessApiController(MilkTeaProcessService milkTeaProcessService, TaskService taskService) {
        this.milkTeaProcessService = milkTeaProcessService;
        this.taskService = taskService;
    }


    @PostMapping("/new-order")
    public String newOrder(@RequestBody MilkTeaOrderDetailDTO payload) {
        return milkTeaProcessService.startMilkTeaProcess(payload);
    }

    @PostMapping(value = "/completeOrder")
    public String nextStep(@RequestBody MilkTeaNextTaskDTO payload) {
        return milkTeaProcessService.nextStep(payload);
    }


    @GetMapping("/getCurrentTaskId")
    public String getTaskId(@RequestParam String processInstanceId) {
        return taskService.createTaskQuery().processInstanceId(processInstanceId).singleResult().getId();
    }

    @GetMapping(value = "/getCurrentTaskById")
    public MilkTeaTaskDTO getTaskById(@RequestParam String taskId) {
        return milkTeaProcessService.getTaskById(taskId);
    }

}
