package vn.lpt.hris.controllers;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import vn.lpt.hris.domains.ResignProcess;
import vn.lpt.hris.services.ResignProcessService;

@RestController
@RequestMapping("/resign-process")
@RequiredArgsConstructor
@Slf4j
public class ResignProcessController {

  private final ResignProcessService resignProcessService;


  @GetMapping(produces = APPLICATION_JSON_VALUE)
  @PageableAsQueryParam
  public Page<ResignProcess> getInstances(
      @RequestParam(value = "assignee", required = false) String assignee,
      @RequestParam(value = "owner", required = false) String owner,
      @RequestParam(value = "ownerName", required = false) String ownerName,
      @RequestParam(value = "status", required = false) String status,
      @Parameter(hidden = true) Pageable pageable) {
    return resignProcessService.search(assignee, owner, ownerName, status, pageable);
  }


}
