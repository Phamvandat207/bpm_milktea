package vn.lpt.hris.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.extern.slf4j.Slf4j;
import org.activiti.api.process.model.ProcessDefinition;
import org.activiti.api.process.model.ProcessInstance;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vn.lpt.hris.domains.ResignProcess;
import vn.lpt.hris.exceptions.BpmException;
import vn.lpt.hris.models.PagingResponse;
import vn.lpt.hris.models.ProcessConfigDTO;
import vn.lpt.hris.models.StartProcessModel;
import vn.lpt.hris.models.TaskCrmDTO;
import vn.lpt.hris.models.common.ErrorDTO;
import vn.lpt.hris.models.processconfig.ProcessCreateDTO;
import vn.lpt.hris.models.processconfig.ProcessDTO;
import vn.lpt.hris.services.ProcessConfigService;
import vn.lpt.hris.services.ProcessService;
import vn.lpt.hris.services.ResignProcessService;
import vn.lpt.hris.services.TaskService;

import java.text.ParseException;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/processes")
@Slf4j
public class ProcessApiController {
    private final ProcessService processService;
    private final TaskService taskService;
    private final ProcessConfigService processConfigService;
    private final ResignProcessService resignProcessService;

    public ProcessApiController(ProcessService processService, TaskService taskService, ProcessConfigService processConfigService, ResignProcessService resignProcessService) {
        this.processService = processService;
        this.taskService = taskService;
        this.processConfigService = processConfigService;
        this.resignProcessService = resignProcessService;
    }


    /**
     * list of ProcessDefinition
     *
     * @author lptech
     * @since 02/02/2020
     */
    @GetMapping(value = "/definitions", produces = APPLICATION_JSON_VALUE)
    public List<ProcessDefinition> getDefinitions() {
        return processService.getDefinitions();
    }

    /**
     * list of ProcessInstance
     *
     * @author lptech
     * @since 02/02/2020
     */
    @GetMapping(value = "/instances", produces = APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<ProcessInstance> getInstances(@Parameter(hidden = true) Pageable pageable) {
        var actPageable =
                org.activiti.api.runtime.shared.query.Pageable.of(
                        (int) pageable.getOffset(), pageable.getPageSize());
        var actPage = processService.getInstances(actPageable);
        return new PageImpl<>(actPage.getContent(), pageable, actPage.getTotalItems());
    }

    /**
     * StartProcessPayload
     *
     * @author lptech
     * @since 02/02/2020
     */
    @PostMapping("/instances")
    public String startProcess(@RequestBody StartProcessModel payload) throws ParseException {
        return processService.startProcess(payload);
    }

    /**
     * Next task
     *
     * @author lptech
     * @since 02/02/2020
     */
    @PostMapping("/nextStep")
    public String nextTask(@RequestBody StartProcessModel payload) {
        return processService.nextTask(payload);
    }

    /**
     * list of Task
     *
     * @author lptech
     * @since 02/02/2020
     */
    @GetMapping(value = "/{processDefinitionId}/tasks", produces = APPLICATION_JSON_VALUE)
    public PagingResponse<TaskCrmDTO> getTasksByProcessInstanceId(@Parameter(hidden = true) Pageable pageable, @PathVariable String processDefinitionId
            , @RequestParam(required = false) String type) {
        return PagingResponse.of(processService.getAllTaskByProcess(processDefinitionId, type,pageable));
    }

    /**
     * Xem chi tiết process definition
     *
     * @param processDefinitionId mã
     * @return ProcessConfig
     * @author KhanhNM
     * @since 13/02/2022
     */
    @GetMapping(value = "/config/{processDefinitionId}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDefinition(@PathVariable String processDefinitionId) {
        try {
            // Lấy chi tiết
            ProcessDTO processDTO = processConfigService.findByProcessId(processDefinitionId);
            return new ResponseEntity<>(processDTO, HttpStatus.OK);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(
                    new ErrorDTO(e.getMessage(), e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Cập nhật ProcessConfig
     *
     * @param id  mã
     * @param dto model update
     * @author KhanhNM
     * @since 13/02/2022
     */
    @PutMapping(value = "/config/{id}", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateDefinition(@PathVariable Long id, @RequestBody
            ProcessCreateDTO dto) {
        try {
            // Cập nhật
            processConfigService.update(dto, id);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Cập nhật config json cho ProcessConfig
     *
     * @author lptech
     * @since 13/02/2022
     */
    @PutMapping(value = "/process-config/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateProcessConfig(@RequestBody ProcessConfigDTO dto) {
        try {
            // Cập nhật
            processConfigService.updateProcessConfig(dto);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    /**
     * Cập nhật config json cho all step của process-config
     *
     * @author lptech
     * @since 13/02/2022
     */
    @PutMapping(value = "/process-config/updateStep", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateStepConfig(@RequestBody ProcessConfigDTO dto) {
        try {
            // Cập nhật
            processConfigService.updateStepConfig(dto);
            return new ResponseEntity<>(HttpStatus.ACCEPTED);
        } catch (BpmException ex) {
            // Lỗi nghiệp vụ trả về 400
            return new ResponseEntity<>(
                    new ErrorDTO(ex.getMessageKey(), ex.getMessage()), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "/resign-process", produces = APPLICATION_JSON_VALUE)
    @PageableAsQueryParam
    public Page<ResignProcess> getInstances(
            @RequestParam(value = "assignee", required = false) String assignee,
            @RequestParam(value = "owner", required = false) String owner,
            @RequestParam(value = "ownerName", required = false) String ownerName,
            @RequestParam(value = "status", required = false) String status,
            @Parameter(hidden = true) Pageable pageable) {
        return resignProcessService.search(assignee, owner, ownerName, status, pageable);
    }
}
